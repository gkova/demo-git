/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica.pratica31;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author gabriel
 */
public class Pratica31 {
    
    public static void main(String[] args) {
        Date inicio = new Date();
        String meuNome = "gabriel KOVALHUK";
        System.out.println(meuNome);
        System.out.println(meuNome.substring(8, 9).toUpperCase() + meuNome.substring(9).toLowerCase() + ", " + meuNome.substring(0, 1).toUpperCase() + ".");
        GregorianCalendar dataNascimento = new GregorianCalendar(1967, Calendar.JUNE, 17);
        Date agora = new Date();        
        System.out.println((agora.getTime() - dataNascimento.getTime().getTime())/86400000L);
        Date fim = new Date();
        System.out.println(fim.getTime() - inicio.getTime());
        
    }
    
}
